package my.imagene.varianteditor.update.significantcarrierpercentile;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.bean.CsvNumber;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportedFileRecord {

    @CsvNumber(value = "#")
    @CsvBindByName(column = "id")
    private Long id;

    @CsvCustomBindByName(column = "ważny", converter = Digit2BooleanConverter.class)
    private Boolean significant;

    @CsvCustomBindByName(column = "nosicielstwo", converter = Digit2BooleanConverter.class)
    private Boolean carrier;

    @CsvCustomBindByName(column = "percentyl", converter = Digit2BooleanConverter.class)
    private Boolean percentile;

}

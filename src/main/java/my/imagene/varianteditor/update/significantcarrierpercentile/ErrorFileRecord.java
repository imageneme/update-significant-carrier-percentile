package my.imagene.varianteditor.update.significantcarrierpercentile;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvRecurse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorFileRecord {

    @CsvRecurse
    private ImportedFileRecord baseFields;

    @CsvBindByName(column = "errors")
    private String errors;

}

package my.imagene.varianteditor.update.significantcarrierpercentile;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@Profile("!test")
public class DataImporter implements CommandLineRunner {

    public static final String UPDATE_SUCCESS = "Flags were updated successfully. :)";
    public static final String NO_ERROR = "OK";
    public static final String ID_NULL_ERROR = "Id musi być wypełnione!";
    public static final String SIGNIFICANT_NULL_ERROR = "Ważność musi być oznaczona!";
    public static final String CARRIER_NULL_ERROR = "Nosiciel musi być oznaczony!";
    public static final String PERCENTILE_NULL_ERROR = "Percentyl musi być oznaczony!";
    public static final String CARRIER_ERROR = "Brak możliwości przypisania nosicielstwa!";
    public static final String MISSED_FACT_IN_DB = "Fakt nie istnieje w bazie danych!";
    public static final String MISSED_FACT_IN_CSV = "Fakt nie został uwzględniony w pliku, natomiast istnieje w bazie danych!";
    public static final String FACT_DUPLICATED = "Fakt z takim id został zdefiniowany w pliku kilkukrotnie!";

    private final List<ImportedFileRecord> dataRecords;
    private final List<Long>
        dbFactIds,
        csvFactIds,
        duplicatedIds;
    private final FactRepository factRepository;
    private final String errorFilePath;
    private final CsvWriterConfigured csvWriter;

    private boolean isFileValid = true;

    public DataImporter(
        List<ImportedFileRecord> dataRecords,
        FactRepository factRepository,
        @Value("${csv.errorPath}") String errorFilePath,
        CsvWriterConfigured csvWriter
    ) {
        this.dataRecords = dataRecords;
        this.factRepository = factRepository;
        this.errorFilePath = errorFilePath;
        this.dbFactIds = factRepository.getFactIds();
        this.csvWriter = csvWriter;

        this.csvFactIds = dataRecords.stream().map(ImportedFileRecord::getId).toList();
        this.duplicatedIds = new ArrayList<>();
    }

    @Override
    public void run(String... args) throws CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, IOException {
        final List<ErrorFileRecord> validatedData = new ArrayList<>(dataRecords.stream().map(this::assignErrors).toList());
        dbFactIds.removeAll(duplicatedIds);
        if (dbFactIds.isEmpty() && isFileValid) {
            factRepository.updateFlags(dataRecords);
            log.info(UPDATE_SUCCESS);
        } else {
            validatedData.addAll(0, dbFactIds.stream().map(this::buildMissedRecord).toList());
            csvWriter.write(validatedData);
            log.error("Data file has errors!\nSee output file: %s".formatted(errorFilePath));
        }
    }

    private ErrorFileRecord assignErrors(@NonNull ImportedFileRecord factFlags) {
        final List<String> errors = new ArrayList<>();

        if (factFlags.getId() == null) {
            errors.add(ID_NULL_ERROR);
        } else {
            if (csvFactIds.stream().filter(id -> Objects.equals(id, factFlags.getId())).count() > 1) {
                errors.add(FACT_DUPLICATED);
                duplicatedIds.add(factFlags.getId());
                if (!dbFactIds.contains(factFlags.getId())) {
                    errors.add(MISSED_FACT_IN_DB);
                }
            } else {
                if (!dbFactIds.remove(factFlags.getId())) {
                    errors.add(MISSED_FACT_IN_DB);
                }
            }
        }

        if (factFlags.getSignificant() == null) {
            errors.add(SIGNIFICANT_NULL_ERROR);
        }

        if (factFlags.getCarrier() == null) {
            errors.add(CARRIER_NULL_ERROR);
        }

        if ((factFlags.getSignificant() != null) &&
            (factFlags.getCarrier() != null)) {
            if (factFlags.getCarrier() && !factFlags.getSignificant()) {
                errors.add(CARRIER_ERROR);
            }
        }

        if (factFlags.getPercentile() == null) {
            errors.add(PERCENTILE_NULL_ERROR);
        }

        final String errorMessage;
        if (errors.isEmpty()) {
            errorMessage = NO_ERROR;
        } else {
            isFileValid = false;
            errorMessage = String.join("\n", errors.toArray(new String[0]));
            log.error("""
                Error was occurred in fact with id=%d!
                %s"""
                .formatted(
                    factFlags.getId(),
                    errorMessage
                ));
        }

        return new ErrorFileRecord(factFlags, errorMessage);

    }

    private ErrorFileRecord buildMissedRecord(Long factId) {
        final ImportedFileRecord unassignedRecord = new ImportedFileRecord();
        unassignedRecord.setId(factId);
        return new ErrorFileRecord(unassignedRecord, MISSED_FACT_IN_CSV);
    }

}

package my.imagene.varianteditor.update.significantcarrierpercentile;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class CsvWriterConfigured {

    private final Writer writer;
    private final StatefulBeanToCsv<ErrorFileRecord> beanToCsv;

    public CsvWriterConfigured(
        @Value("${csv.charset}") String charset,
        @Value("${csv.separator}") Character separator,
        @Value("${csv.quote}") Character quote,
        @Value("${csv.escape}") Character escape,
        @Value("${csv.errorPath}") String errorPath
    ) throws IOException {
        writer = new FileWriter(errorPath, Charset.forName(charset));
        beanToCsv = new StatefulBeanToCsvBuilder<ErrorFileRecord>(writer)
            .withSeparator(separator)
            .withEscapechar(escape)
            .withQuotechar(quote)
            .build();
    }

    public void write(List<ErrorFileRecord> beans) throws CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, IOException {
        beanToCsv.write(beans);
        writer.close();
    }

}

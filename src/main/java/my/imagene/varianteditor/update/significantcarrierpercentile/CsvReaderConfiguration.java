package my.imagene.varianteditor.update.significantcarrierpercentile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Log4j2
@Configuration
@ConstructorBinding
public class CsvReaderConfiguration {

    private final Charset charset;
    private final Character separator;
    private final Character quote;
    private final Character escape;
    private final String importPath;

    public CsvReaderConfiguration(
        @Value("${csv.charset}") String charset,
        @Value("${csv.separator}") Character separator,
        @Value("${csv.quote}") Character quote,
        @Value("${csv.escape}") Character escape,
        @Value("${csv.importPath}") String importPath
    ) {
        this.charset = Charset.forName(charset);
        this.separator = separator;
        this.quote = quote;
        this.escape = escape;
        this.importPath = importPath;
    }

    @Bean
    @Scope("prototype")
    public FileReader fileReader() {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(importPath, charset);
        } catch (IOException e) {
            log.error(e.getMessage());
            log.error("""
                $error_message
                Place import file in this location: "$inputFilePath\""""
                .replace("$error_message", e.getMessage())
                .replace("$inputFilePath", importPath)
            );
            System.exit(1);
        }
        return fileReader;
    }

    @Bean
    @Scope("prototype")
    public CsvToBean<ImportedFileRecord> csvReader(FileReader fileReader) {
        return new CsvToBeanBuilder<ImportedFileRecord>(fileReader)
            .withType(ImportedFileRecord.class)
            .withSeparator(separator)
            .withEscapeChar(escape)
            .withQuoteChar(quote)
            .withStrictQuotes(false)
            .withIgnoreEmptyLine(true)
            .withIgnoreLeadingWhiteSpace(true)
            .withOrderedResults(false)
            .build();
    }

    @Bean
    public List<ImportedFileRecord> dataList(CsvToBean<ImportedFileRecord> csvReader) {
        return csvReader.parse();
    }

    @Bean
    @Scope("prototype")
    public Stream<ImportedFileRecord> dataStream(CsvToBean<ImportedFileRecord> csvReader) {
        return csvReader.stream();
    }

    @Bean
    @Scope("prototype")
    public Iterator<ImportedFileRecord> dataIterator(CsvToBean<ImportedFileRecord> csvReader) {
        return csvReader.iterator();
    }

}

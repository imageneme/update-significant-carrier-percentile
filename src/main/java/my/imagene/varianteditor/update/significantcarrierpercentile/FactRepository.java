package my.imagene.varianteditor.update.significantcarrierpercentile;

import static jooq.tables.Variantfact.VARIANTFACT;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class FactRepository {

    private final DSLContext dsl;

    private final static String FIND_FACTS_QUERY = "SELECT DISTINCT vf.id "
        + "FROM variant v "
        + "         LEFT JOIN variant_variantfact vvf "
        + "                   ON vvf.variant_alt = v.alt AND vvf.variant_chrom = v.chrom AND vvf.variant_position = v.position AND "
        + "                      vvf.variant_ref = v.ref "
        + "         LEFT JOIN variantgroup vg ON v.group_name = vg.name "
        + "         LEFT JOIN variantgroup_variantfact vgvf ON v.group_name = vgvf.variantgroup_name "
        + "         LEFT JOIN variant_rsid vrs ON vrs.variant_alt = v.alt AND vrs.variant_chrom = v.chrom AND vrs.variant_position = v.position AND "
        + "                                       vrs.variant_ref = v.ref "
        + "         LEFT JOIN variant_info vi ON vi.variant_alt = v.alt AND vi.variant_chrom = v.chrom AND vi.variant_position = v.position AND "
        + "                                      vi.variant_ref = v.ref AND vi.info_key = 'GENEINFO' "
        + "         JOIN variantfact vf ON vvf.facts_id = vf.id OR vgvf.facts_id = vf.id "
        + "         JOIN factcategory c1 on vf.category_id = c1.id "
        + "         JOIN factcategory c2 on c1.parent_id = c2.id "
        + "         JOIN factcategory c3 on c2.parent_id = c3.id "
        + "WHERE v.state = 2;";

    List<Long> getFactIds() {
        return dsl.fetch(FIND_FACTS_QUERY).into(Long.class);
    }

    void updateFlags(Collection<ImportedFileRecord> dataToImport) {
        dsl
            .batch(dataToImport
                .stream()
                .map(this::toUpdateQuery)
                .collect(Collectors.toList()))
            .execute();
    }

    private Query toUpdateQuery(ImportedFileRecord fact) {
        return dsl
            .update(VARIANTFACT)
            .set(VARIANTFACT.SIGNIFICANT, fact.getSignificant())
            .set(VARIANTFACT.CARRIER, fact.getCarrier())
            .set(VARIANTFACT.PERCENTILE, fact.getPercentile())
            .where(VARIANTFACT.ID.eq(fact.getId()));
    }

}

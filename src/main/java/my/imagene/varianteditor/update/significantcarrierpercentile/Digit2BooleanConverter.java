package my.imagene.varianteditor.update.significantcarrierpercentile;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class Digit2BooleanConverter extends AbstractBeanField<ImportedFileRecord, String> {

    @Override
    protected Boolean convert(String value) throws CsvDataTypeMismatchException {
        if (value == null || value.isBlank()) {
            return null;
        }

        return switch (value.toUpperCase()) {
            case "TRUE", "PRAWDA", "1" -> Boolean.TRUE;
            case "FALSE", "FAŁSZ", "0" -> Boolean.FALSE;
            default -> throw new CsvDataTypeMismatchException(String.format("Value \"%s\" cannot be parsed into TRUE/FALSE!", value));
        };
    }

}

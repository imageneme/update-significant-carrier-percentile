/*
 * This file is generated by jOOQ.
 */

package jooq.tables.records;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import jooq.tables.Varianttoprocess;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record8;
import org.jooq.Row8;
import org.jooq.impl.UpdatableRecordImpl;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VarianttoprocessRecord extends UpdatableRecordImpl<VarianttoprocessRecord>
    implements Record8<Long, String, String, String, Long, String, Long, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.varianttoprocess.id</code>.
     */
    public VarianttoprocessRecord setId(Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.id</code>.
     */
    @NotNull
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.varianttoprocess.state</code>.
     */
    public VarianttoprocessRecord setState(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.state</code>.
     */
    public String getState() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.varianttoprocess.variant_alt</code>.
     */
    public VarianttoprocessRecord setVariantAlt(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.variant_alt</code>.
     */
    @Size(max = 1048576)
    public String getVariantAlt() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.varianttoprocess.variant_chrom</code>.
     */
    public VarianttoprocessRecord setVariantChrom(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.variant_chrom</code>.
     */
    @Size(max = 255)
    public String getVariantChrom() {
        return (String) get(3);
    }

    /**
     * Setter for <code>public.varianttoprocess.variant_position</code>.
     */
    public VarianttoprocessRecord setVariantPosition(Long value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.variant_position</code>.
     */
    public Long getVariantPosition() {
        return (Long) get(4);
    }

    /**
     * Setter for <code>public.varianttoprocess.preferreduser</code>.
     */
    public VarianttoprocessRecord setPreferreduser(String value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.preferreduser</code>.
     */
    @Size(max = 255)
    public String getPreferreduser() {
        return (String) get(5);
    }

    /**
     * Setter for <code>public.varianttoprocess.modificationtime</code>.
     */
    public VarianttoprocessRecord setModificationtime(Long value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.modificationtime</code>.
     */
    public Long getModificationtime() {
        return (Long) get(6);
    }

    /**
     * Setter for <code>public.varianttoprocess.variant_ref</code>.
     */
    public VarianttoprocessRecord setVariantRef(String value) {
        set(7, value);
        return this;
    }

    /**
     * Getter for <code>public.varianttoprocess.variant_ref</code>.
     */
    @Size(max = 1048576)
    public String getVariantRef() {
        return (String) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record8 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row8<Long, String, String, String, Long, String, Long, String> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    @Override
    public Row8<Long, String, String, String, Long, String, Long, String> valuesRow() {
        return (Row8) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return Varianttoprocess.VARIANTTOPROCESS.ID;
    }

    @Override
    public Field<String> field2() {
        return Varianttoprocess.VARIANTTOPROCESS.STATE;
    }

    @Override
    public Field<String> field3() {
        return Varianttoprocess.VARIANTTOPROCESS.VARIANT_ALT;
    }

    @Override
    public Field<String> field4() {
        return Varianttoprocess.VARIANTTOPROCESS.VARIANT_CHROM;
    }

    @Override
    public Field<Long> field5() {
        return Varianttoprocess.VARIANTTOPROCESS.VARIANT_POSITION;
    }

    @Override
    public Field<String> field6() {
        return Varianttoprocess.VARIANTTOPROCESS.PREFERREDUSER;
    }

    @Override
    public Field<Long> field7() {
        return Varianttoprocess.VARIANTTOPROCESS.MODIFICATIONTIME;
    }

    @Override
    public Field<String> field8() {
        return Varianttoprocess.VARIANTTOPROCESS.VARIANT_REF;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getState();
    }

    @Override
    public String component3() {
        return getVariantAlt();
    }

    @Override
    public String component4() {
        return getVariantChrom();
    }

    @Override
    public Long component5() {
        return getVariantPosition();
    }

    @Override
    public String component6() {
        return getPreferreduser();
    }

    @Override
    public Long component7() {
        return getModificationtime();
    }

    @Override
    public String component8() {
        return getVariantRef();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getState();
    }

    @Override
    public String value3() {
        return getVariantAlt();
    }

    @Override
    public String value4() {
        return getVariantChrom();
    }

    @Override
    public Long value5() {
        return getVariantPosition();
    }

    @Override
    public String value6() {
        return getPreferreduser();
    }

    @Override
    public Long value7() {
        return getModificationtime();
    }

    @Override
    public String value8() {
        return getVariantRef();
    }

    @Override
    public VarianttoprocessRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value2(String value) {
        setState(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value3(String value) {
        setVariantAlt(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value4(String value) {
        setVariantChrom(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value5(Long value) {
        setVariantPosition(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value6(String value) {
        setPreferreduser(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value7(Long value) {
        setModificationtime(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord value8(String value) {
        setVariantRef(value);
        return this;
    }

    @Override
    public VarianttoprocessRecord values(Long value1, String value2, String value3, String value4, Long value5, String value6, Long value7,
        String value8) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VarianttoprocessRecord
     */
    public VarianttoprocessRecord() {
        super(Varianttoprocess.VARIANTTOPROCESS);
    }

    /**
     * Create a detached, initialised VarianttoprocessRecord
     */
    public VarianttoprocessRecord(Long id, String state, String variantAlt, String variantChrom, Long variantPosition, String preferreduser,
        Long modificationtime, String variantRef) {
        super(Varianttoprocess.VARIANTTOPROCESS);

        setId(id);
        setState(state);
        setVariantAlt(variantAlt);
        setVariantChrom(variantChrom);
        setVariantPosition(variantPosition);
        setPreferreduser(preferreduser);
        setModificationtime(modificationtime);
        setVariantRef(variantRef);
    }
}

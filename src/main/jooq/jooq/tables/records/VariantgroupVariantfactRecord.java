/*
 * This file is generated by jOOQ.
 */

package jooq.tables.records;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import jooq.tables.VariantgroupVariantfact;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VariantgroupVariantfactRecord extends TableRecordImpl<VariantgroupVariantfactRecord> implements Record2<String, Long> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.variantgroup_variantfact.variantgroup_name</code>.
     */
    public VariantgroupVariantfactRecord setVariantgroupName(String value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variantfact.variantgroup_name</code>.
     */
    @NotNull
    @Size(max = 1048576)
    public String getVariantgroupName() {
        return (String) get(0);
    }

    /**
     * Setter for <code>public.variantgroup_variantfact.facts_id</code>.
     */
    public VariantgroupVariantfactRecord setFactsId(Long value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variantfact.facts_id</code>.
     */
    @NotNull
    public Long getFactsId() {
        return (Long) get(1);
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<String, Long> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<String, Long> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return VariantgroupVariantfact.VARIANTGROUP_VARIANTFACT.VARIANTGROUP_NAME;
    }

    @Override
    public Field<Long> field2() {
        return VariantgroupVariantfact.VARIANTGROUP_VARIANTFACT.FACTS_ID;
    }

    @Override
    public String component1() {
        return getVariantgroupName();
    }

    @Override
    public Long component2() {
        return getFactsId();
    }

    @Override
    public String value1() {
        return getVariantgroupName();
    }

    @Override
    public Long value2() {
        return getFactsId();
    }

    @Override
    public VariantgroupVariantfactRecord value1(String value) {
        setVariantgroupName(value);
        return this;
    }

    @Override
    public VariantgroupVariantfactRecord value2(Long value) {
        setFactsId(value);
        return this;
    }

    @Override
    public VariantgroupVariantfactRecord values(String value1, Long value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VariantgroupVariantfactRecord
     */
    public VariantgroupVariantfactRecord() {
        super(VariantgroupVariantfact.VARIANTGROUP_VARIANTFACT);
    }

    /**
     * Create a detached, initialised VariantgroupVariantfactRecord
     */
    public VariantgroupVariantfactRecord(String variantgroupName, Long factsId) {
        super(VariantgroupVariantfact.VARIANTGROUP_VARIANTFACT);

        setVariantgroupName(variantgroupName);
        setFactsId(factsId);
    }
}

/*
 * This file is generated by jOOQ.
 */

package jooq.tables.daos;

import java.util.List;
import jooq.tables.DiseaseTranslation;
import jooq.tables.records.DiseaseTranslationRecord;
import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DiseaseTranslationDao extends DAOImpl<DiseaseTranslationRecord, jooq.tables.pojos.DiseaseTranslation, Integer> {

    /**
     * Create a new DiseaseTranslationDao without any configuration
     */
    public DiseaseTranslationDao() {
        super(DiseaseTranslation.DISEASE_TRANSLATION, jooq.tables.pojos.DiseaseTranslation.class);
    }

    /**
     * Create a new DiseaseTranslationDao with an attached configuration
     */
    public DiseaseTranslationDao(Configuration configuration) {
        super(DiseaseTranslation.DISEASE_TRANSLATION, jooq.tables.pojos.DiseaseTranslation.class, configuration);
    }

    @Override
    public Integer getId(jooq.tables.pojos.DiseaseTranslation object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchRangeOfId(Integer lowerInclusive, Integer upperInclusive) {
        return fetchRange(DiseaseTranslation.DISEASE_TRANSLATION.ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchById(Integer... values) {
        return fetch(DiseaseTranslation.DISEASE_TRANSLATION.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public jooq.tables.pojos.DiseaseTranslation fetchOneById(Integer value) {
        return fetchOne(DiseaseTranslation.DISEASE_TRANSLATION.ID, value);
    }

    /**
     * Fetch records that have <code>language BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchRangeOfLanguage(String lowerInclusive, String upperInclusive) {
        return fetchRange(DiseaseTranslation.DISEASE_TRANSLATION.LANGUAGE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>language IN (values)</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchByLanguage(String... values) {
        return fetch(DiseaseTranslation.DISEASE_TRANSLATION.LANGUAGE, values);
    }

    /**
     * Fetch records that have <code>translated BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchRangeOfTranslated(String lowerInclusive, String upperInclusive) {
        return fetchRange(DiseaseTranslation.DISEASE_TRANSLATION.TRANSLATED, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>translated IN (values)</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchByTranslated(String... values) {
        return fetch(DiseaseTranslation.DISEASE_TRANSLATION.TRANSLATED, values);
    }

    /**
     * Fetch records that have <code>disease_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchRangeOfDiseaseId(Integer lowerInclusive, Integer upperInclusive) {
        return fetchRange(DiseaseTranslation.DISEASE_TRANSLATION.DISEASE_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>disease_id IN (values)</code>
     */
    public List<jooq.tables.pojos.DiseaseTranslation> fetchByDiseaseId(Integer... values) {
        return fetch(DiseaseTranslation.DISEASE_TRANSLATION.DISEASE_ID, values);
    }
}

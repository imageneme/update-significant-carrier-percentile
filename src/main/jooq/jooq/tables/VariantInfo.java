/*
 * This file is generated by jOOQ.
 */

package jooq.tables;

import java.util.Arrays;
import java.util.List;
import jooq.Keys;
import jooq.Public;
import jooq.tables.records.VariantInfoRecord;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row6;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VariantInfo extends TableImpl<VariantInfoRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>public.variant_info</code>
     */
    public static final VariantInfo VARIANT_INFO = new VariantInfo();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VariantInfoRecord> getRecordType() {
        return VariantInfoRecord.class;
    }

    /**
     * The column <code>public.variant_info.variant_alt</code>.
     */
    public final TableField<VariantInfoRecord, String> VARIANT_ALT =
        createField(DSL.name("variant_alt"), SQLDataType.VARCHAR(1048576).nullable(false), this, "");

    /**
     * The column <code>public.variant_info.variant_chrom</code>.
     */
    public final TableField<VariantInfoRecord, String> VARIANT_CHROM =
        createField(DSL.name("variant_chrom"), SQLDataType.VARCHAR(255).nullable(false), this, "");

    /**
     * The column <code>public.variant_info.variant_position</code>.
     */
    public final TableField<VariantInfoRecord, Long> VARIANT_POSITION =
        createField(DSL.name("variant_position"), SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>public.variant_info.info</code>.
     */
    public final TableField<VariantInfoRecord, String> INFO = createField(DSL.name("info"), SQLDataType.VARCHAR(1048576), this, "");

    /**
     * The column <code>public.variant_info.info_key</code>.
     */
    public final TableField<VariantInfoRecord, String> INFO_KEY =
        createField(DSL.name("info_key"), SQLDataType.VARCHAR(1048576).nullable(false), this, "");

    /**
     * The column <code>public.variant_info.variant_ref</code>.
     */
    public final TableField<VariantInfoRecord, String> VARIANT_REF = createField(DSL.name("variant_ref"),
        SQLDataType.VARCHAR(1048576).nullable(false).defaultValue(DSL.field("''::character varying", SQLDataType.VARCHAR)), this, "");

    private VariantInfo(Name alias, Table<VariantInfoRecord> aliased) {
        this(alias, aliased, null);
    }

    private VariantInfo(Name alias, Table<VariantInfoRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>public.variant_info</code> table reference
     */
    public VariantInfo(String alias) {
        this(DSL.name(alias), VARIANT_INFO);
    }

    /**
     * Create an aliased <code>public.variant_info</code> table reference
     */
    public VariantInfo(Name alias) {
        this(alias, VARIANT_INFO);
    }

    /**
     * Create a <code>public.variant_info</code> table reference
     */
    public VariantInfo() {
        this(DSL.name("variant_info"), null);
    }

    public <O extends Record> VariantInfo(Table<O> child, ForeignKey<O, VariantInfoRecord> key) {
        super(child, key, VARIANT_INFO);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public UniqueKey<VariantInfoRecord> getPrimaryKey() {
        return Keys.VARIANT_INFO_PKEY;
    }

    @Override
    public List<UniqueKey<VariantInfoRecord>> getKeys() {
        return Arrays.<UniqueKey<VariantInfoRecord>>asList(Keys.VARIANT_INFO_PKEY);
    }

    @Override
    public List<ForeignKey<VariantInfoRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<VariantInfoRecord, ?>>asList(Keys.VARIANT_INFO__VARIANT_FK);
    }

    private transient Variant _variant;

    public Variant variant() {
        if (_variant == null) {
            _variant = new Variant(this, Keys.VARIANT_INFO__VARIANT_FK);
        }

        return _variant;
    }

    @Override
    public VariantInfo as(String alias) {
        return new VariantInfo(DSL.name(alias), this);
    }

    @Override
    public VariantInfo as(Name alias) {
        return new VariantInfo(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public VariantInfo rename(String name) {
        return new VariantInfo(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VariantInfo rename(Name name) {
        return new VariantInfo(name, null);
    }

    // -------------------------------------------------------------------------
    // Row6 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row6<String, String, Long, String, String, String> fieldsRow() {
        return (Row6) super.fieldsRow();
    }
}

/*
 * This file is generated by jOOQ.
 */

package jooq.tables.pojos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VariantgroupVariantfact implements Serializable {

    private static final long serialVersionUID = 1L;

    private String variantgroupName;
    private Long factsId;

    public VariantgroupVariantfact() {}

    public VariantgroupVariantfact(VariantgroupVariantfact value) {
        this.variantgroupName = value.variantgroupName;
        this.factsId = value.factsId;
    }

    public VariantgroupVariantfact(
        String variantgroupName,
        Long factsId
    ) {
        this.variantgroupName = variantgroupName;
        this.factsId = factsId;
    }

    /**
     * Getter for <code>public.variantgroup_variantfact.variantgroup_name</code>.
     */
    @NotNull
    @Size(max = 1048576)
    public String getVariantgroupName() {
        return this.variantgroupName;
    }

    /**
     * Setter for <code>public.variantgroup_variantfact.variantgroup_name</code>.
     */
    public VariantgroupVariantfact setVariantgroupName(String variantgroupName) {
        this.variantgroupName = variantgroupName;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variantfact.facts_id</code>.
     */
    @NotNull
    public Long getFactsId() {
        return this.factsId;
    }

    /**
     * Setter for <code>public.variantgroup_variantfact.facts_id</code>.
     */
    public VariantgroupVariantfact setFactsId(Long factsId) {
        this.factsId = factsId;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VariantgroupVariantfact (");

        sb.append(variantgroupName);
        sb.append(", ").append(factsId);

        sb.append(")");
        return sb.toString();
    }
}

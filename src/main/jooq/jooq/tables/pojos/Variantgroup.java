/*
 * This file is generated by jOOQ.
 */

package jooq.tables.pojos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Variantgroup implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private Integer operator;
    private Long factId;
    private String notes;

    public Variantgroup() {}

    public Variantgroup(Variantgroup value) {
        this.name = value.name;
        this.operator = value.operator;
        this.factId = value.factId;
        this.notes = value.notes;
    }

    public Variantgroup(
        String name,
        Integer operator,
        Long factId,
        String notes
    ) {
        this.name = name;
        this.operator = operator;
        this.factId = factId;
        this.notes = notes;
    }

    /**
     * Getter for <code>public.variantgroup.name</code>.
     */
    @NotNull
    @Size(max = 1048576)
    public String getName() {
        return this.name;
    }

    /**
     * Setter for <code>public.variantgroup.name</code>.
     */
    public Variantgroup setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup.operator</code>.
     */
    public Integer getOperator() {
        return this.operator;
    }

    /**
     * Setter for <code>public.variantgroup.operator</code>.
     */
    public Variantgroup setOperator(Integer operator) {
        this.operator = operator;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup.fact_id</code>.
     */
    public Long getFactId() {
        return this.factId;
    }

    /**
     * Setter for <code>public.variantgroup.fact_id</code>.
     */
    public Variantgroup setFactId(Long factId) {
        this.factId = factId;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup.notes</code>.
     */
    @Size(max = 1048576)
    public String getNotes() {
        return this.notes;
    }

    /**
     * Setter for <code>public.variantgroup.notes</code>.
     */
    public Variantgroup setNotes(String notes) {
        this.notes = notes;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Variantgroup (");

        sb.append(name);
        sb.append(", ").append(operator);
        sb.append(", ").append(factId);
        sb.append(", ").append(notes);

        sb.append(")");
        return sb.toString();
    }
}

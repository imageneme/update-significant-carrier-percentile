/*
 * This file is generated by jOOQ.
 */

package jooq.tables.pojos;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VariantgroupVariant implements Serializable {

    private static final long serialVersionUID = 1L;

    private String variantgroupName;
    private String variantsAlt;
    private String variantsChrom;
    private Long variantsPosition;
    private String variantsRef;

    public VariantgroupVariant() {}

    public VariantgroupVariant(VariantgroupVariant value) {
        this.variantgroupName = value.variantgroupName;
        this.variantsAlt = value.variantsAlt;
        this.variantsChrom = value.variantsChrom;
        this.variantsPosition = value.variantsPosition;
        this.variantsRef = value.variantsRef;
    }

    public VariantgroupVariant(
        String variantgroupName,
        String variantsAlt,
        String variantsChrom,
        Long variantsPosition,
        String variantsRef
    ) {
        this.variantgroupName = variantgroupName;
        this.variantsAlt = variantsAlt;
        this.variantsChrom = variantsChrom;
        this.variantsPosition = variantsPosition;
        this.variantsRef = variantsRef;
    }

    /**
     * Getter for <code>public.variantgroup_variant.variantgroup_name</code>.
     */
    @NotNull
    @Size(max = 1048576)
    public String getVariantgroupName() {
        return this.variantgroupName;
    }

    /**
     * Setter for <code>public.variantgroup_variant.variantgroup_name</code>.
     */
    public VariantgroupVariant setVariantgroupName(String variantgroupName) {
        this.variantgroupName = variantgroupName;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variant.variants_alt</code>.
     */
    @NotNull
    @Size(max = 1048576)
    public String getVariantsAlt() {
        return this.variantsAlt;
    }

    /**
     * Setter for <code>public.variantgroup_variant.variants_alt</code>.
     */
    public VariantgroupVariant setVariantsAlt(String variantsAlt) {
        this.variantsAlt = variantsAlt;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variant.variants_chrom</code>.
     */
    @NotNull
    @Size(max = 255)
    public String getVariantsChrom() {
        return this.variantsChrom;
    }

    /**
     * Setter for <code>public.variantgroup_variant.variants_chrom</code>.
     */
    public VariantgroupVariant setVariantsChrom(String variantsChrom) {
        this.variantsChrom = variantsChrom;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variant.variants_position</code>.
     */
    @NotNull
    public Long getVariantsPosition() {
        return this.variantsPosition;
    }

    /**
     * Setter for <code>public.variantgroup_variant.variants_position</code>.
     */
    public VariantgroupVariant setVariantsPosition(Long variantsPosition) {
        this.variantsPosition = variantsPosition;
        return this;
    }

    /**
     * Getter for <code>public.variantgroup_variant.variants_ref</code>.
     */
    @Size(max = 1048576)
    public String getVariantsRef() {
        return this.variantsRef;
    }

    /**
     * Setter for <code>public.variantgroup_variant.variants_ref</code>.
     */
    public VariantgroupVariant setVariantsRef(String variantsRef) {
        this.variantsRef = variantsRef;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VariantgroupVariant (");

        sb.append(variantgroupName);
        sb.append(", ").append(variantsAlt);
        sb.append(", ").append(variantsChrom);
        sb.append(", ").append(variantsPosition);
        sb.append(", ").append(variantsRef);

        sb.append(")");
        return sb.toString();
    }
}

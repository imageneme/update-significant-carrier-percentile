package my.imagene.varianteditor.update.significantcarrierpercentile


import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

@Transactional
@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource("classpath:test.properties")
abstract class TestConfiguration extends Specification {}

package my.imagene.varianteditor.update.significantcarrierpercentile


import org.springframework.beans.factory.annotation.Value

class DataImporterTest extends TestConfiguration {

    CsvWriterConfigured csvWriterMock = Mock()

    FactRepository factRepositoryMock = Mock()

    @Value('${csv.errorPath}')
    String path

    def mockedImporter(List inputData) {
        return new DataImporter(inputData, factRepositoryMock, path, csvWriterMock)
    }

    def dbData() {
        return [
                new ImportedFileRecord(1, false, false, false),
                new ImportedFileRecord(2, true, false, false),
                new ImportedFileRecord(3, true, true, false),
                new ImportedFileRecord(4, true, true, true),
                new ImportedFileRecord(5, false, false, true),
        ]
    }

    def noErrors(List<ImportedFileRecord> data) {
        return data.stream().map(d -> new ErrorFileRecord(d, DataImporter.NO_ERROR)).toList()
    }

    def "Should update valid data"() {
        given:
        def inputData = dbData()

        when:
        mockedImporter(inputData).run()

        then:
        1 * factRepositoryMock.getFactIds() >> dbData().id
        1 * factRepositoryMock.updateFlags(inputData)
        0 * csvWriterMock.write(_)
    }

    def "Should validate carrier flag"() {
        given:
        def inputData = dbData()
        inputData.each { it.carrier = true }

        def expectedOutput = noErrors(inputData)
        expectedOutput[0].errors = DataImporter.CARRIER_ERROR
        expectedOutput[4].errors = DataImporter.CARRIER_ERROR

        when:
        mockedImporter(inputData).run()

        then:
        1 * factRepositoryMock.getFactIds() >> dbData().id
        0 * factRepositoryMock.updateFlags(_)
        1 * csvWriterMock.write({ it.sort { it.baseFields.id } == expectedOutput.sort { it.baseFields.id } })
    }

    def "Should check required values"() {
        given:
        def inputData = dbData()
        inputData[0].id = null
        inputData[1].significant = null
        inputData[2].carrier = null
        inputData[3].percentile = null

        def expectedOutput = noErrors(inputData)
        expectedOutput.add(new ErrorFileRecord(
                new ImportedFileRecord(1, null, null, null),
                DataImporter.MISSED_FACT_IN_CSV))
        expectedOutput[0].errors = DataImporter.ID_NULL_ERROR
        expectedOutput[1].errors = DataImporter.SIGNIFICANT_NULL_ERROR
        expectedOutput[2].errors = DataImporter.CARRIER_NULL_ERROR
        expectedOutput[3].errors = DataImporter.PERCENTILE_NULL_ERROR

        when:
        mockedImporter(inputData).run()

        then:
        1 * factRepositoryMock.getFactIds() >> dbData().id
        0 * factRepositoryMock.updateFlags(_)
        1 * csvWriterMock.write({ it.sort { it.baseFields.id } == expectedOutput.sort { it.baseFields.id } })
    }

    def "Should find duplicates in file"() {
        given:
        def inputData = dbData()
        inputData[0].id = 1
        inputData[1].id = 1

        def expectedOutput = noErrors(inputData)
        expectedOutput.add(new ErrorFileRecord(
                new ImportedFileRecord(2, null, null, null),
                DataImporter.MISSED_FACT_IN_CSV))
        expectedOutput[0].errors = DataImporter.FACT_DUPLICATED
        expectedOutput[1].errors = DataImporter.FACT_DUPLICATED

        when:
        mockedImporter(inputData).run()

        then:
        1 * factRepositoryMock.getFactIds() >> dbData().id
        0 * factRepositoryMock.updateFlags(_)
        1 * csvWriterMock.write({ it.sort { it.baseFields.id } == expectedOutput.sort { it.baseFields.id } })
    }

    def "Should find missing facts in file"() {
        given:
        def inputData = dbData()
        inputData.remove(0)

        def expectedOutput = noErrors(dbData())
        expectedOutput[0].each {
            it.baseFields = new ImportedFileRecord(1, null, null, null)
            it.errors = DataImporter.MISSED_FACT_IN_CSV
        }

        when:
        mockedImporter(inputData).run()

        then:
        1 * factRepositoryMock.getFactIds() >> dbData().id
        0 * factRepositoryMock.updateFlags(_)
        1 * csvWriterMock.write({ it.sort { it.baseFields.id } == expectedOutput.sort { it.baseFields.id } })
    }

    def "Should find missing facts in db"() {
        given:
        def dbData = this.dbData()
        dbData.remove(0)

        def inputData = this.dbData()

        def expectedOutput = noErrors(inputData)
        expectedOutput[0].errors = DataImporter.MISSED_FACT_IN_DB

        when:
        mockedImporter(inputData).run()

        then:
        1 * factRepositoryMock.getFactIds() >> dbData.id
        0 * factRepositoryMock.updateFlags(_)
        1 * csvWriterMock.write({ it.sort { it.baseFields.id } == expectedOutput.sort { it.baseFields.id } })
    }

}
